(ns ui.main
  (:require [reagent.core :as r]
            [ui.layout :as layout]
            [ui.events :as events]
            [re-frame.core :as rf]))

(defn init
  []
  (js/console.log "starting scramblies challange UI...")
  (r/render-component [layout/main-layout] (.getElementById js/document "container")))
