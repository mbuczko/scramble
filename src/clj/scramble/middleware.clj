(ns scramble.middleware
  (:require [clojure.tools.logging :as log]
            [prone.middleware :refer [wrap-exceptions]]
            [ring.middleware.format :refer [wrap-restful-format]]
            [selmer.middleware :refer [wrap-error-page]]))

(defn wrap-middlewares
  "Applies bunch of middlewares useful in dev environment."

  [handler]
  (log/info "applying development middlewares")
  (-> handler
      (wrap-error-page)
      (wrap-exceptions {:app-namespaces ["scramble"]})
      (wrap-restful-format :formats [:json-kw]
                           :response-options {:json-kw {:pretty true}})))
