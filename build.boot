(set-env!
 :source-paths   #{"src/clj" "src/cljs" "test"}
 :resource-paths #{"resources" "assets"}
 :dependencies   '[[org.clojure/clojure "1.9.0"]
                   [org.clojure/clojurescript "1.10.339"]
                   [org.clojure/tools.namespace "0.3.0-alpha4"]
                   [org.clojure/tools.nrepl "0.2.13"]
                   [org.clojure/tools.logging "0.4.0"]
                   [adzerk/boot-cljs "2.1.4" :scope "test"]
                   [metosin/bat-test "0.4.0" :scope "test"]
                   [powerlaces/boot-figreload "0.5.14" :scope "test"]
                   [ring/ring-defaults "0.3.2"]
                   [ch.qos.logback/logback-classic "1.2.3"]
                   [ring-middleware-format "0.7.2"]
                   [http-kit "2.3.0"]
                   [compojure "1.6.1"]
                   [mount "0.1.13"]
                   [selmer "1.12.0"]
                   [prone "1.6.0"]
                   [reagent "0.8.1"]
                   [re-frame "0.10.5"]
                   [cljs-ajax "0.7.3"]
                   [day8.re-frame/http-fx "0.1.4"]])

(def +version+ "0.0.1")

(require
 '[clojure.tools.namespace.repl]
 '[powerlaces.boot-figreload :refer [reload]]
 '[metosin.bat-test :refer [bat-test]]
 '[adzerk.boot-cljs :refer [cljs]]
 '[scramble.system])


;; which source dirs should be monitored for changes when resetting app?
(apply clojure.tools.namespace.repl/set-refresh-dirs (get-env :source-paths))

(deftask go
  []
  (scramble.system/go))

(deftask build
  []
  (cljs))

(deftask dev
  []
  (comp (watch)
        (notify)
        (reload)
        (build)))

(task-options! cljs {:source-map true :optimizations :none}
               pom  {:project 'mbuczko/scramble
                     :version +version+
                     :description "Scramblies challange"})
