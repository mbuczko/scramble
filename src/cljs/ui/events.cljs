(ns ui.events
  (:require [ajax.core :as ajax]
            [day8.re-frame.http-fx]
            [re-frame.core :as rf]))

(rf/reg-event-fx
 :search
 (fn [cofx [_ [str1 str2]]]
   (let [db (:db cofx)]
     {:http-xhrio {:uri "/scramble"
                   :params (-> {}
                               (cond-> str1 (assoc :str1 str1))
                               (cond-> str2 (assoc :str2 str2)))
                   :method :get
                   :format (ajax/json-request-format)
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success [::string-found]
                   :on-failure [::error]}})))

(rf/reg-event-db
 ::string-found
 (fn [db [_ response]]
   (-> db
       (dissoc :error)
       (assoc  :result (str (:result response))))))

(rf/reg-event-db
 ::error
 (fn [db [_ error]]
   (-> db
       (dissoc :result)
       (assoc  :error (-> error :response :error)))))
