(ns scramble.core)

(defn frequencies-reducer
  "Based on input character `ch` decreases corresponding frequency in `freqs` map.
  Returns altered frequencies map."

  [freqs ch]
  (if-let [frequency (freqs ch)]
    (assoc freqs ch (dec frequency))
    freqs))

(defn scramble?
  "Returns true if a portion of non-empty str1 characters can be rearranged to match
  non-empty str2. Returns false otherwise.

  This implements an idea of looking for scrambled substring in other string by
  comparing frequencies of characters occurencies. To calculate a result following 3
  steps are performed:

   1. frequency map of str2 gets calculated first
   2. reducing of str1 goes next, decreasing frequency map each time a correspoding character is met
   3. frequency map gets finally filtered to check if there are any frequencies > 0 left

  Anything left after filtering indicates mismatch in frequencies thus returns a false."

  [str1 str2]
  (let [input (sort str1)
        freqs (into (sorted-map)
                    (frequencies str2))]
    (and (seq str2)
         (->> input
              (reduce frequencies-reducer freqs)
              (filter #(> (second %) 0))
              empty?))))
