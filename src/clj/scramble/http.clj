(ns scramble.http
  (:require [clojure.tools.logging :as log]
            [compojure.core :refer [defroutes GET routes]]
            [compojure.route :as route]
            [mount.core :as mount :refer [defstate]]
            [org.httpkit.server :as web]
            [ring.middleware.defaults :refer [api-defaults wrap-defaults]]
            [scramble.core]
            [scramble.middleware]
            [selmer.parser :as selmer]))

(defn scramble-handler
  "HTTP handler of /scramble path.

  Requires str1 and str2 query parameters to respond with 200 OK.
  Otherwise 400 Bad Request is sent back."

  [str1 str2]
  (log/infof "/scramble str1=%s str2=%s" str1 str2)

  (if (or (empty? str1)
          (empty? str2))
    {:status 400
     :body {:error "Missing input parameters."}}
    {:status 200
     :body {:result (scramble.core/scramble? str1 str2)}}))

(defroutes api-routes
  (GET "/scramble" [str1 str2] (scramble-handler str1 str2)))

(defroutes static-routes
  (GET "/" [] (selmer/render-file "index.html" {}))
  (route/resources "/styles"  {:root "styles"})
  (route/resources "/scripts" {:root "scripts"})
  (route/resources "/")
  (route/not-found "Not Found"))

(defn init-routes []
  (scramble.middleware/wrap-middlewares
   (routes (wrap-defaults api-routes api-defaults) static-routes)))

(defn init-server
  "Starts up HTTP server on port 8080."

  []
  (log/info "starting HTTP server on port 8080")
  (web/run-server (init-routes) {:port 8080}))

(defstate http-server
  :start (init-server)
  :stop  (http-server))
