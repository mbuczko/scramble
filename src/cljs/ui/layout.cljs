(ns ui.layout
  (:require [reagent.core :as r]
            [re-frame.core :as rf]
            [clojure.string :as str]))

(rf/reg-sub :result
 (fn [db _]
   (:result db)))

(rf/reg-sub :error
 (fn [db _]
   (:error db)))

(defn main-layout []
  (let [strings (r/atom [nil nil])
        result  (rf/subscribe [:result])
        error   (rf/subscribe [:error])]

    (fn []
      [:div
       [:form
        [:input {:placeholder "String to search in" :on-change #(swap! strings assoc 0 (-> % .-target .-value))}]
        [:input {:placeholder "String to match" :on-change #(swap! strings assoc 1 (-> % .-target .-value))}]
        [:input {:type "button"
                 :value "Search"
                 :on-click #(rf/dispatch [:search @strings])}]

        (when @result
          [:p "Strings matching? " [:span {:class (str "match-" @result)} (str/upper-case @result)]])

        (when @error
          [:p "Error while looking for string match: " [:span.error @error]])]])))
