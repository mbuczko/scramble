(ns scramble.core-test
  (:require [scramble.core :refer [scramble?]]
            [clojure.test :refer :all]))

(deftest scrambled-substrings
  (testing "scrambled substring found in a string"
    (is (scramble? "rekqodlw" "world"))
    (is (scramble? "cedewaraaossoqqyt" "codewars"))
    (is (scramble? "wewewewew" "wewew")))

  (testing "scrambled substring missed in a string"
    (is (not (scramble? nil "steak")))
    (is (not (scramble? "steak" nil)))
    (is (not (scramble? "steak" "")))
    (is (not (scramble? "" "steak")))
    (is (not (scramble? "katas" "steak")))
    (is (not (scramble? "cedevaraaossoqqyt" "codewars")))))
