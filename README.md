# Scramblies challange

Checks if portion of input string can be rearranged to match the other string.

Built with use of [boot tool](http://boot-clj.com/) with [reload workflow](http://thinkrelevance.com/blog/2013/06/04/clojure-workflow-reloaded) in mind (based on glorious [mount](https://github.com/tolitius/mount)).

## Usage
To run tests:

```sh
boot bat-test
```

To get repl and auto-started HTTP server (at http://localhost:8080):

```sh
boot repl build go
```

To get REPL in dev mode (cljs auto-build and refresh enabled):

```sh
boot repl dev
```

To restart system from already started up REPL:

```clojure
boot.user=> (require '[scramble.system :as system])
boot.user=> (system/reset)
```

## API

Started HTTP server exposes one GET endpoint at `/scramble` path which requires 2 parameters: `str1` and `str2`. Example:

```sh
curl "http://localhost:8080/scramble?str1=rekqodlw&str2=world" -H 'Accept: application/json'
```

In case of match/mismatch of given strings a `HTTP 200 OK` is returned with corresponding result. Missing parameter(s) causes `HTTP 400 Bad Request`.

## Licence

GNU General Public License v3.0

